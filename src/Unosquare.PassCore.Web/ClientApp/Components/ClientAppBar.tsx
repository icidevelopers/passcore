import AppBar from '@material-ui/core/AppBar/AppBar';
import Grid from '@material-ui/core/Grid/Grid';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';
import Typography from '@material-ui/core/Typography/Typography';
import HelpIcon from '@material-ui/icons/Help';
import * as React from 'react';
import { GlobalContext } from '../Provider/GlobalContext';
import iciLogo from '../assets/images/password_reset_banner.png';


export const ClientAppBar: React.FunctionComponent<any> = () => {
    const { changePasswordForm, changePasswordTitle } = React.useContext(GlobalContext);
    const { helpText } = changePasswordForm;

    return (

            <Grid
                container={true}
                style={{ height: '64px', width: '100%' }}
                direction="row"
                justify="space-between"
                alignItems="center"
            >
                <Typography
                    variant="h6"
                    color="secondary"
                 
                >
                    {changePasswordTitle}
                <img src={iciLogo} style={{ margin: '0 10px 0 380px', maxHeight: '100px', width: '940px', height:'110px' }} />
                </Typography>
            </Grid>
    );
};
